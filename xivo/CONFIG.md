# Configuration de XiVO

## Installation d'un certificat

Afin d'exploiter au mieux les différentes fonctionnalités de XiVO basées sur le protocole HTTP, il peut être intéressant de remplacer le certificat par défaut (non fiable), par notre propre certificat de confiance.

Plus d'informations ici&nbsp;: https://documentation.xivo.solutions/en/stable/xivo/system/https_certificate.html

## Assistant de configuration de XiVO

Une fois les services installés ([Installation](INSTALL.md)) et le certificat déployé ([Installation d'un certificat](#installation-dun-certificat)), afin de pouvoir utiliser XiVO, il est nécessaire d'établir les configurations de base des services.

Pour cela, il suffit de se rendre dans l'assistant de configuration de XiVO en utilisant un navigateur HTTP au moyen de l'adresse IP du serveur, par exemple&nbsp;: [https://xivo.miletrie.chl/](https://xivo.miletrie.chl/).

### Langue et licence

Les deux premières étapes permettent de configurer les paramètres linguistiques et la validation des conditions de la licence GNU GPLv3.

### Configuration

Cette étape est relativement importante puisqu'elle permettra à XiVO de définir les paramètres d'écoutes de ses différents services (à noter toutefois que toute cette configuration est sûrement déjà pré-remplie). Le mot de passe à saisir correspondra à celui qui sera associé à l'utilisateur d'administration de l'interface WEB, à savoir l'utilisateur `root`&nbsp;:

![installation_1_configuration][]

### Entités et contexte

L'entité représente en quelque sorte le "contexte" d'installation de notre IPBX, il peut par exemple symboliser le site sur lequel l'IPBX est installé, il s'agit de donner un nom au périmètre de déploiement de notre téléphonie.

**Attention&nbsp;:** Il vaut mieux privilégier un nom composé de caractères non-accentués (`[A-Za-z0-9-.]`). Ici, `Milétrie`, puisqu'il s'agit du nom affiché, n'est pas rejeté par les filtres, mais sa valeur réellement exploitée devra être `miletrie` et non `milétrie` au risque de générer des problèmes au niveau d'Asterisk, il sera donc nécessaire de modifier cette valeur par la suite.

Le reste de la configuration pourra se faire plus tard dans l'interface, mais il est tout de même possible de définir la plage de numérotation interne pour gagner du temps par la suite&nbsp;:

![installation_2_entites_contextes][]

### Validation

Enfin, il suffit de vérifier les informations saisies et de valider la configuration pour que l'assistant de configuration procède à la configuration des services.

![installation_3_rappel][]

## Configuration du provisioning

Cette section consiste à configurer les services de XiVO afin de fournir un provisioning automatique (mode `autoprov`) des postes téléphoniques Polycom (VVX300, VVX400 et VVX500) en exploitant les services DHCP et le service `provd` de XiVO (plus d'information [ici](https://documentation.xivo.solutions/en/stable/xivo/administration/provisioning/introduction.html)).

### Configuration du service DHCP

La configuation du service DHCP est très limitée et ne concerne que la définition de la plage d'adresse IP (IPv4 a priori) à allouer aux postes&nbsp;:

![configuration_1_dhcp][]

### Configuration du NTP pour les terminaisons

Afin de d'assurer d'une certaine cohérence pour les communications téléphoniques, il peut être intéressant d'indiquer un serveur de temps (NTP) afin de garantir la synchronisation entre chaque terminaison (poste téléphonique)&nbsp;:

![configuration_2_modele_terminaison][]

### Configuration de `provd`

Pour faire du provisioning, XiVO dispose d'un service appelé `provd` qui va permettre, au moyen de greffons spécifiques à chaque type de postes, de définir les paramètres à provisionner au niveau des postes téléphoniques.

Afin de permettre l'installation de ces greffons, il est nécessaire de configurer les paramètres d'accès au *repository* contenant ces greffons (p. ex.; URL, Proxy)&nbsp;:

![configuration_3_approvisionnement][]

**Note&nbsp;:** Pour l'installation de plugins en version `testing`, `archive` ou développés par la communauté, voir [Alternative plugins repository](https://documentation.xivo.solutions/en/stable/xivo/administration/provisioning/basic_configuration.html#alternative-plugins-repository).

### Installation du greffon Polycom

Le greffon `xivo-polycom-5.4.3` permet la prise en charge du provisioning des postes Polycom au niveau du *bootloader* (*bootblock* ou unité d'amorçage) et du *firmware* (*software* ou système). Pour installer le greffon et ses composants, il suffit de choisir l'action installer (bouton ![installer-greffon][])&nbsp;:

![configuration_4_greffon_polycom][]

Une fois installé, redémarrer ou brancher un poste téléphonique Polycom sur le réseau de tel sorte qu'il obtienne une adresse IP du serveur DHCP de XiVO.

Le poste devrait alors apparaître dans la liste des terminaisons en mode `autoprov` (icone ![configure][])&nbsp;:

![configuration_5_terminaisons][]

**Notes&nbsp;:**

 1. Si le poste est affiché en mode non configuré (icone ![non_configure][]), le greffon associé à la terminaison n'est probablement pas bien installé&nbsp;;
 2. Si le greffon est bien installé, il est probable que le service DHCP ne soit pas configuré pour reconnaitre et "router" (*callback*) l'adresse MAC associée à la terminaison, si cela n'est pas déjà le cas, procéder au téléchargement des fichiers de configuration du service DHCP&nbsp;:

 ```bash
 dhcpd-update --download
 ```

 3. Si le greffon et le service DHCP sont à jour, vérifier les sous-classes de `voip-mac-address-prefix` dans le fichier `/etc/dhcp/dhcpd_update/polycom.conf`, par exemple `64:16:7f:91:47:8b` est pris en compte par la sous-classe&nbsp;:

 ```java
 subclass "voip-mac-address-prefix" 1:64:16:7f {
     option tftp-server-name = concat(config-option VOIP.http-server-uri, "/000000000000.cfg");
     log(concat("[", binary-to-ascii(16, 8, ":", hardware), "] ", "BOOT Polycom PREFIX 1:64:16:7f"));
 }
 ```

 4. Si le téléphone n'apparaît pas, vérifier l'accès des téléphones à l'API de XiVO (p. ex.; https://xivo.miletrie.chl/service/ipbx/index.php/pbx_settings/devices/) et l'efficacité réseau du service DHCP (éviter de déployer le service DHCP sur un domaine de diffusion où un autre est déjà en service).

## Ajout d'un utilisateur

Un utilisateur au niveau de l'interface de XiVO correspond effectivement à un utilisateur réel, une terminaison correspond au poste téléphonique et une ligne à son numéro. L'utilisateur peut être associé à une ligne et une ligne associée à une terminaison.

### Création d'un l'utilisateur

![configuration_6_utilisateur][]

### Création d'une ligne

![configuration_7_utilisateur_ligne][]

**Note&nbsp;:** Il doit être possible pour un utilisateur de posséder plus d'une ligne (colonne `Ligne (N°)`), cependant, l'interface HTTP (pas seulement le formulaire), ne semble autoriser l'ajout que d'une seule ligne&hellip;

### Configuration de la ligne (facultatif)

En utilisant la console Javascript du navigateur (ou Burp Suite 😉), on peut modifier les données du formulaire en lecture seule afin de modifier le nom de la ligne et le mot de passe associé à cette ligne qui sont générés aléatoirement (possible que cette possibilité disparaisse dans les versions ultérieures à la release `2017 LTS1 (2017.03.02)`)&nbsp;:

```javascript
$(".it-disabled").removeClass("it-disabled").removeAttr("readonly");
```

![configuration_8_ligne][]

### Synchronisation de la terminaison

Une fois la ligne créée et associée à la terminaison, cette dernière devrait avoir basculée du mode `autoprov` (icone ![autoprov][]) au mode configuré (icone ![configure][]), il ne restera qu'à synchroniser la terminaison (bouton ![synchronize][]).

![configuration_9_terminaisons][]

## Interconnexion SIP

Pour l'exemple, l'utilisation du serveur OpenIMS est faite afin d'établir une interconnexion SIP entre deux serveurs XiVO (une seule des configuration est illustrée, l'autre est très proche et ne diffère que dans les identifiants de connexion et les SDA qui lui sont associées).

Cet exemple admet les données suivantes&nbsp;:

| Paramètre                | Valeur                            | Description                                             |
|--------------------------|-----------------------------------|---------------------------------------------------------|
| Identifiant de connexion | xivoloc@open-ims.test             | Identifiant correspondant à l'IMSU inscrit dans OpenIMS |
| Mot de passe             | 0000                              | Mot de passe simple pour les tests uniquement !         |
| Type de connexion        | Peer                              | Peer pour une liaison trunk bidirectionnelle            |
| Serveur                  | pcscf.open-ims.test               | Proxy-Call Session Control Function de OpenIMS          |
| Codecs                   | G.711 A-law, G.711 µ-law et G.722 | Codecs supportés lors de la communication voix          |

**Quickfix&nbsp;:** Pour retirer la valeur par défaut du port de connexion associé au trunk SIP dans le formulaire `Interconnexion SIP` onglet `Avancé` (cette valeur doit être vide pour être traitée par OpenIMS&hellip; Par exemple, `sip:0549511001@open-ims.test:5060` n'est pas reconnu par le service S-SCSCF et doit correspondre à `sip:0549511001@open-ims.test`)&nbsp;:

```bash
sed -i -r 's/(\$array\['"'"'element'"'"'\]\['"'"'port'"'"'\]\['"'"'default'"'"'\][ ]*=[ ]*)[^;]+(;)/\1""\2/' /usr/share/xivo-web-interface/object/service/ipbx/asterisk/trunksip/config.inc
```

Résultat&nbsp;:

```diff
@@ -241,7 +241,7 @@ $array['element']['host-type']['value'] = array('dynamic','static');
 $array['element']['host-type']['unset'] = true;

 $array['element']['port'] = array();
-$array['element']['port']['default'] = 5060;
+$array['element']['port']['default'] = "";
 $array['element']['port']['null'] = true;

 $array['element']['regexten'] = array();
```

### Création d'une interconnexion SIP (*SIP trunk*)

Plus d'informations ici&nbsp;: https://documentation.xivo.solutions/en/stable/xivo/administration/interconnections/xivo_with_voip_provider.html#establish-the-trunk

![interconnexion_sip_1_general][]
![interconnexion_sip_2_enregistrement][]
![interconnexion_sip_3_signalisation][]
![interconnexion_sip_4_avance][]

Ce formulaire a permit à au service `confgend` de XiVO de configurer Asterisk afin de créer un enregistrement SIP avec la directive `registrer`. Cette directive respecte généralement ce format et s'inscrit dans le fichier `sip.conf` du serveur Asterisk&nbsp;:

```
register => user[:secret[:authuser]]@host[:port][/extension]
```

Pour voir cette configuration, il est possible d'intéroger le service de configuration d'Asterisk en indiquant le fichier de configuration à afficher (ici `sip.conf`)&nbsp;:

```bash
xivo-confgen asterisk/sip.conf | grep -E "^register[ ]*=[>]?"
```

Ce qui donne dans cet exemple&nbsp;:

```
register => udp://xivo@open-ims.test:0000:xivo@open-ims.test@pcscf.open-ims.test:4060
```

### Configurations des appels sortants

Configuration du contexte des appels internes pour inclure les appels sortants&nbsp;:

![contextes_1_appels_internes][]

Création d'une gestion des appels sortants exploitant le *SIP trunk*&nbsp;:

![appels_sortants_1_general][]

Configuration de l'extension à faire passer en appels sortants (ici on retire le 9 devant le numéro en indiquant `Stripnum = 1`)&nbsp;:

**Note&nbsp;:** On peut par exemple considérer le support des numéros saisies avec le `+33`&nbsp;:

 - **Préfixe externe&nbsp;:** 0
 - **Extension&nbsp;:** `+339549511XXX`
 - **Stripnum&nbsp;:** 4

Résultat&nbsp;: On compose le `+339549511000`, le numéro effectivement appelé sera le `0549511000` et la demande de connexion SIP sera faite à destination de `sip:0549511000@open-ims.test`

![appels_sortants_2_extensions][]

### Configurations des appels entrants

Configuration du contexte des appels entrants pour inclure les numéros correspondant à nos SDA&nbsp;:

![contextes_2_appels_entrants][]

Configuration d'une SDA pour appeller&nbsp;:

![appels_entrants_1_sda][]

Résultat&nbsp;: On compose le `+339549511000` depuis un poste connecté au second serveur XiVO (configuré comme celui-ci, mais avec ses propres identifiants de connexion SIP au réseau IMS et ses propres SDA), le numéro effectivement appelé sera le `0549511000`, la demande de connexion SIP sera faite à destination de `sip:0549511000@open-ims.test`, le serveur XiVO actuel recevra la demande d'appel et la transférera alors vers John DOE.

<!-- Icones -->

 [installer-greffon]: illustrations/installer-greffon.png
 [non_configure]: illustrations/non_configure.png
 [autoprov]: illustrations/autoprov.png
 [configure]: illustrations/configure.png
 [synchronize]: illustrations/synchronize.png

<!-- Captures d'écran -->

 [installation_1_configuration]: illustrations/installation_1_configuration.png
 [installation_2_entites_contextes]: illustrations/installation_2_entites_contextes.png
 [installation_3_rappel]: illustrations/installation_3_rappel.png
 [configuration_1_dhcp]: illustrations/configuration_1_dhcp.png
 [configuration_2_modele_terminaison]: illustrations/configuration_2_modele_terminaison.png
 [configuration_3_approvisionnement]: illustrations/configuration_3_approvisionnement.png
 [configuration_4_greffon_polycom]: illustrations/configuration_4_greffon_polycom.png
 [configuration_5_terminaisons]: illustrations/configuration_5_terminaisons.png
 [configuration_6_utilisateur]: illustrations/configuration_6_utilisateur.png
 [configuration_7_utilisateur_ligne]: illustrations/configuration_7_utilisateur_ligne.png
 [configuration_8_ligne]: illustrations/configuration_8_ligne.png
 [configuration_9_terminaisons]: illustrations/configuration_9_terminaisons.png
 [interconnexion_sip_1_general]: illustrations/interconnexion_sip_1_general.png
 [interconnexion_sip_2_enregistrement]: illustrations/interconnexion_sip_2_enregistrement.png
 [interconnexion_sip_3_signalisation]: illustrations/interconnexion_sip_3_signalisation.png
 [interconnexion_sip_4_avance]: illustrations/interconnexion_sip_4_avance.png
 [contextes_1_appels_internes]: illustrations/contextes_1_appels_internes.png
 [appels_sortants_1_general]: illustrations/appels_sortants_1_general.png
 [appels_sortants_2_extensions]: illustrations/appels_sortants_2_extensions.png
 [contextes_2_appels_entrants]: illustrations/contextes_2_appels_entrants.png
 [appels_entrants_1_sda]: illustrations/appels_entrants_1_sda.png
