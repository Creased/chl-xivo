# Installation de XiVO sur Debian Jessie

## Installation

Téléchargement du script d'installation&nbsp;:

```bash
mkdir /opt/xivo/
wget http://mirror.xivo.solutions/xivo_install.sh -O /opt/xivo/install.sh
```

Mise à jour des permissions d'accès UNIX&nbsp;:

```bash
pushd /opt/xivo/
chmod +x ./install.sh
```

Installation de XiVO&nbsp;:

```bash
./install.sh
```

Configuration du proxy pour le provisioning DHCP&nbsp;:

```bash
sed -i -r 's@([;] )?(http: ).+@\2'${HTTP_PROXY}'@' /etc/xivo/dhcpd-update.conf
```

Mise à jour des fichiers de configurations pour le provisioning DHCP (téléchargement des configurations depuis [http://provd.xivo.solutions/xivo/dhcpd-update](http://provd.xivo.solutions/xivo/dhcpd-update))&nbsp;:

```bash
dhcpd-update --download
```
