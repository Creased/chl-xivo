# Installation et configuration d'un cœur de réseau IMS (OpenSIPS)

## Installation du système

Voir [Installation de Debian Jessie](BASE.md) avec&nbsp;:

```bash
export HOSTNAME=opensips
export DOMAIN=miletrie.chl
export NIC=eth0
export IP=172.28.128.17
export MASK=255.255.255.0
export GW=172.28.128.1
export DNS1=172.16.10.32
export DNS2=172.16.10.33
export PROXY=172.18.4.1:8080
export USR=user
export GRP=users
export USRID=1337
export LANG=fr_FR.UTF-8
export LOCALE=Europe/Paris
```

## Installation d'OpenSIPS

Définition des variables contextuelles à l'installation d'OpenSIPS&nbsp;:

```bash
export IP=$(hostname -i | head -n1)
export DB_USR=opensips
export DB_PWD=SB9jKHbePmDk3v5Z
export DB_HOST=php-dev.miletrie.chl
export DB_PORT=3306
export DB_NAME=opensips
export DB_URI=mysql://${DB_USR}:${DB_PWD}@${DB_HOST}:${DB_PORT}/${DB_NAME}
export RTP_SOCK=udp:127.0.0.1:22222

export IP_IPBX_A=172.28.128.16
export IP_IPBX_B=10.16.10.61
export HOSTNAME_IPBX_A=xivo
export HOSTNAME_IPBX_B=oxe
export TEL_IPBX_A="5701 5702"
export TEL_IPBX_B="5719"
```

Vérification des variables&nbsp;:

```bash
VAR="IP DB_USR DB_PWD DB_HOST DB_PORT DB_NAME DB_URI DOMAIN RTP_SOCK IP_IPBX_A IP_IPBX_B HOSTNAME_IPBX_A HOSTNAME_IPBX_B TEL_IPBX_A TEL_IPBX_B"
for V in ${VAR}; do (
    printf "%-20s => %s\n" "${V}" "${!V}"
); done
```

### Installation de la chaîne de compilation

```bash
apt-get update
apt-get -fy upgrade
apt-get -fy dist-upgrade
apt-get -fy install \
    subversion wget git \
    libtool automake autoconf gcc g++ make \
    openssl libssl-dev \
    libncurses5-dev build-essential \
    bison flex perl libdbi-perl \
    libdbd-mysql-perl \
    libterm-readline-gnu-perl \
    libpcre3 libpcre3-dev libxml2 libxml2-dev
```

### Installation de Apache MySQL PHP

```bash
apt-get -fy install \
    mysql-server libmysqlclient-dev mysql-client \
    apache2 libapache2-mod-php5 php5 php5-cli php5-gd \
    php5-xmlrpc php5-mysql php-pear php5-xmlrpc php5-curl
```

### Installation du proxy RTP

```bash
apt-get -fy install rtpproxy
cat <<-EOF >/etc/default/rtpproxy
# Defaults for rtpproxy

# The control socket.
#CONTROL_SOCK="unix:/var/run/rtpproxy/rtpproxy.sock"
# To listen on an UDP socket, uncomment this line:
CONTROL_SOCK=${RTP_SOCK}

# Additional options that are passed to the daemon.
EXTRA_OPTS="-l 127.0.0.1"

EOF
systemctl restart rtpproxy.service
```

### Installation d'OpenSIPS

```bash
git clone https://github.com/OpenSIPS/opensips.git -b 2.3
wget https://github.com/OpenSIPS/opensips/archive/2.3.tar.gz -O opensips_2.3.0.orig.tar.gz
pushd opensips/
make deb
popd
dpkg -i \
     opensips_2.3.0-1_amd64.deb \
     opensips-dialplan-module_2.3.0-1_amd64.deb \
     opensips-http-modules_2.3.0-1_amd64.deb \
     opensips-mysql-module_2.3.0-1_amd64.deb \
     opensips-presence-modules_2.3.0-1_amd64.deb \
     opensips-xml-module_2.3.0-1_amd64.deb
```

## Configuration d'OpenSIPS

Génération d'une configuration résidentielle pour OpenSIPS&nbsp;:

```bash
osipsconfig  # Generate OpenSIPS Script -> Residential Script -> Configure Residential Script -> ["USE_ALIASES", "USE_AUTH", "USE_DBACC", "USE_DBUSRLOC", "USE_DIALOG", "USE_MULTIDOMAIN", "USE_NAT", "USE_PRESENCE", "USE_DIALPLAN", "VM_DIVERSION", "USE_HTTP_MANAGEMENT_INTERFACE"] -[q]> Save Residential Script -> Generate Residential Script -[q -> q]> Exit & Save All Changes
cp /etc/opensips/opensips.cfg{,.bak}
cp $(ls -tr /etc/opensips/opensips_residential_$(date +%Y-%-m-%-d_%-H)* | tail -n1) /etc/opensips/opensips.cfg
```

Mise à jour des configurations&nbsp;:

```bash
sed -r -i '
    s#mysql://[^"]+#'${DB_URI}'#;
    s@127\.0\.0\.[0-9]+@'${IP}'@;
    s@localhost@127.0.0.1@;
    s@(modparam\("rtpproxy", "rtpproxy_sock", ")[^"]+("\))@\1'${RTP_SOCK}'\2@;
    /^route\[relay\] \{/,/^\}/{
        /if \(is_method\("INVITE"\)\) \{/,/t_on_failure\("missed_call"\)/{
            s@(if \(isflagset\(NAT\))(\) \{)@\1 \&\& has_body("application/sdp")\2@p
        }
    };
    /^onreply_route\[handle_nat\] \{/,/^\}/{
        s@(if \( isflagset\(NAT\))( \))@\1 \&\& has_body("application/sdp")\2@p
    }
' /etc/opensips/opensips.cfg
sed -r -i '
    s@^(# )?(SIP_DOMAIN=).+$@\2'${DOMAIN}'@;
    s@^(# )?(DBENGINE=).+$@\2MYSQL@;
    s@^(# )?(DBPORT=).+$@\2'${DB_PORT}'@;
    s@^(# )?(DBHOST=).+$@\2'${DB_HOST}'@;
    s@^(# )?(DBNAME=).+$@\2'${DB_NAME}'@;
    s@^(# )?(DBRWUSER=).+$@\2'${DB_USR}'@;
    s@^(# )?(DBRWPW=).+$@\2"'${DB_PWD}'"@;
    s@^(# )?(DBROOTUSER=).+$@\2"'${DB_USR}'"@;
    s@^(# )?(VERBOSE=).+$@\21@
' /etc/opensips/opensipsctlrc
```

Création des bases de données&nbsp;:

```bash
opensipsdbctl drop
opensipsdbctl create
opensipsdbctl pframework create
opensipsdbctl extra
```

Redémarrage d'OpenSIPS&nbsp;:

```bash
opensipsctl restart
```

## Ajout d'utilisateurs au réseau IMS

Création d'un domaine sur le réseau IMS&nbsp;:

```bash
opensipsctl domain add ${DOMAIN}
```

### IPBX A

Ajout d'un utilisateur pour l'IPBX A&nbsp;:

```bash
opensipsctl add ${HOSTNAME_IPBX_A}@${DOMAIN} 0000
```

Ajout des SDA pour l'IPBX A&nbsp;:

```bash
for TEL in ${TEL_IPBX_A}; do
opensipsctl alias_db add ${TEL}@${DOMAIN} ${TEL_IPBX_A}@${DOMAIN}
done
```

### IPBX B

Ajout d'un utilisateur pour l'IPBX B&nbsp;:

```bash
opensipsctl add ${HOSTNAME_IPBX_B}@${DOMAIN} 0000
```

Ajout des SDA pour l'IPBX B&nbsp;:

```bash
for TEL in ${TEL_IPBX_B}; do
opensipsctl alias_db add ${TEL}@${DOMAIN} ${TEL_IPBX_B}@${DOMAIN}
done
```

### Affichage des utilisateurs connectés

```bash
opensipsctl ul show
opensipsctl online
```

## Installation d'OpenSIPS Control Panel

Installation des modules PHP pour OSIPS CP&nbsp;:

```bash
php5enmod xmlrpc gd mysql
pear install MDB2 MDB2#mysql log
```

Configuration de PHP&nbsp;:

```bash
sed -r -i '
    s@^(; )?(short_open_tag = ).+$@\2On@
' /etc/php5/apache2/php.ini
```

Installation d'OSIPS CP&nbsp;:

```bash
pushd /var/www/html/
git clone https://github.com/OpenSIPS/opensips-cp.git
```

Installation des bases de données&nbsp;:

```bash
mysql -h ${DB_HOST} -u ${DB_USR} -p${DB_PWD} ${DB_NAME} <./opensips-cp/config/tools/admin/add_admin/ocp_admin_privileges.mysql
mysql -h ${DB_HOST} -u ${DB_USR} -p${DB_PWD} ${DB_NAME} -e "INSERT INTO ocp_admin_privileges (username,password,ha1,available_tools,permissions) values ('admin','admin',md5('admin:admin'),'all','all');"
mysql -h ${DB_HOST} -u ${DB_USR} -p${DB_PWD} ${DB_NAME} <./opensips-cp/config/tools/system/smonitor/tables.mysql
```

Installation du cron pour les statistiques&nbsp;:

```
cp ./opensips-cp/config/tools/system/smonitor/opensips_stats_cron /etc/cron.d/
popd
```

Une fois l'installation du CP effectuée, il est possible d'administrer les utilisateurs, afficher les alias, etc. Pour accéder au CP, il suffit de se rendre sur `http://${HOSTNAME}.${DOMAIN}/opensips-cp/web` et de s'autentifier (par défaut `admin:admin`)&nbsp;:

![opensips_user_contact][]

**Note&nbsp;:** L'illustration ci-dessus permet d'afficher le contact lié à un utilisateur de notre coeur de réseau, il s'agit en fait de l'identité propre au serveur ayant demandé un enregistrement sur cet utilisateur. Cette identité est celle qui sera utilisée pour établir les connexions lors d'une demande d'appel.

![opensips_aliases][]

**Note&nbsp;:** Les alias sont utilisés pour créer différentes identités sur un enregistrement (Une analogie simple avec les noms de domaines qui masquent une adresse IP, ici les alias (SDA) masquent le nom d'utilisateur utilisé par le noeud SIP pour établir son authentification sur le trunk).

L'authentification et la procédure d'appel permet de décrire un flux réseau semblable à celui-ci (à noter qu'ici, la demande d'appel échoue puisque l'utilisateur appelé n'est pas assigné)&nbsp;:

![sip_registration][]

<!-- Captures d'écran -->

 [opensips_user_contact]: illustrations/opensips_user_contact.png
 [opensips_aliases]: illustrations/opensips_aliases.png
 [sip_registration]: illustrations/sip_registration.png
