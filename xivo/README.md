# Mise en place d'une infrastrucutre de communication téléphonique SIP

 - [Debian Jessie](BASE.md)&nbsp;: Installation de Debian Jessie&nbsp;;
 - [Installation](INSTALL.md)&nbsp;: Installation de XiVO sur Debian Jessie&nbsp;;
 - [OpenIMS](OPENIMS.md) **OU** [OpenSIPS](OPENSIPS.md)&nbsp;: Installation et configuration d'un cœur de réseau IMS&nbsp;;
 - [Configuration](CONFIG.md)&nbsp;: Configuration de XiVO pour la communication SIP.
