# Installation et configuration d'un cœur de réseau IMS (OpenIMS)

## Installation du système

Voir [Installation de Debian Jessie](BASE.md) avec&nbsp;:

```bash
export HOSTNAME=openims
export DOMAIN=open-ims.test
export NIC=eth0
export IP=172.16.57.237
export MASK=255.255.0.0
export GW=172.16.10.1
export DNS1=172.16.10.32
export DNS2=172.16.10.33
export PROXY=172.18.4.1:8080
export USR=user
export GRP=users
export USRID=1337
export LANG=fr_FR.UTF-8
export LOCALE=Europe/Paris
```

## Installation d'OpenIMS

Définition des variables contextuelles à l'installation d'OpenIMS&nbsp;:

```bash
export DEBIAN_FRONTEND=noninteractive
export DB_PASSWD=root
export IP_IPBX_A=172.28.128.16
export IP_IPBX_B=172.16.57.155
export HOSTNAME_IPBX_A=xivo
export HOSTNAME_IPBX_B=xivoloc
export TEL_IPBX_A="0549511000 0549511001"
export TEL_IPBX_B="0549511010 0549511011 0549511012"
```

### Installation de la chaîne de compilation

#### Analyseurs lexicaux et syntaxiques&nbsp;:

Composantes du début de la chaîne de compilation, les analyseurs consistent à décomposer les chaînes de caractères en entités lexicales *(&laquo;&nbsp;tokens&nbsp;&raquo;)* en se basant sur des expressions rationnelles, définissant des séquences de caractères autorisés. Ce processus s'appelle la segmentation *(&laquo;&nbsp;tokenization&nbsp;&raquo;)*. Les plus utilisés sont&nbsp;:

 - **GNU Bison&nbsp;:** Compilateur de compilateur chargé de l'analyse sémantique et syntaxique&nbsp;
 - **Flex&nbsp;:** Analyseur de motifs lexicaux.

```bash
apt-get -fy install bison flex
```

#### Autotools

Autotools désigne un ensemble d'outils de build du projet GNU, parmi lesquels on retrouve&nbsp;:

 - **GNU Make&nbsp;:** Fournit une aide à la compilation et à l'édition de liens par la création de fichier de description d'installation des dépendances appelés *makefile*&nbsp;;
 - **GNU AutoMake&nbsp;:** Permet la génération d'un *makefile* à partir d'une description de plus haut niveau&nbsp;;
 - **GNU AutoConf&nbsp;:** Permet la génération d'un script shell de configuration de l'environnement de développement &laquo;&nbsp;configure&nbsp;&raquo; à partir de programmes basés sur le préprocesseur GNU M4&nbsp;;
 - **GNU LibTool&nbsp;:** Utilisé avec AutoConf et AutoMake afin de simplifier le processus de compilation, indépendamment du système utilisé, par la création de bibliothèques dynamiques dépendantes au processus d'installation&nbsp;;
 - **GNU AutoGen&nbsp;:** Fournit une approche similaire à Flex dans la génération de fichiers *makefile*.

```bash
apt-get -fy install make automake autoconf libtool autogen m4
```

#### Compilateur

Le compilateur GCC *(&laquo;&nbsp;GNU Compiler Collection&nbsp;&raquo;)* est un ensemble de compilateur notamment utilisé pour le langage C et C++. En complément de cette bibliothèque de compilation, il est parfois nécessaire d'installer d'autre outils pouvant être utilisées dans la chaîne de compilation&nbsp;:

 - **GNU BinUtils&nbsp;:** Ensemble d'outils permettant la création et la gestion de programmes binaires et sources assembleur&nbsp;;
 - **GNU Debugger&nbsp;:** Fournit une large ensemble d'outils pour tracer ou altérer l'exécution d'un programme *(&laquo;&nbsp;debugging&nbsp;&raquo;)*&nbsp;;
 - **GNU C++&nbsp;:** Compilateur pour C++.

```bash
apt-get -fy install binutils binutils binutils-dev gcc gcc-multilib g++ g++-multilib gdb build-essential linux-headers-$(uname -r)
```

### Installation des dépendances

Afin de fonctionner, *OpenIMS* utilise principalement 4 bibliothèques *(&laquo;&nbsp;libraries&nbsp;&raquo;)* et nécessitera l'installation d'outils spécifiques&nbsp;:

 - **LibXML&nbsp;:** Fournit des fonctions pour l'analyse (« parsing ») de documents XML ;
 - **LibMySQL&nbsp;:** Fournit des fonctions et mécanismes pour l'interfaçage avec une base de données MySQL ;
 - **LibCURL&nbsp;:** Fournit des fonctions d'interface pour le transfert de données exploitant le protocole HTTP ;
 - **LibIPSec&nbsp;:** Fournit des fonctions d'interaction réseau pour la création de tunnels IPSec ;
 - **Apache Ant&nbsp;:** Comparable à Apache Maven ou encore GNU Make, Apache Ant fournit une aide à la compilation par la création de fichier de description détaillée (au format XML) comprenant notamment le processus de compilation, de résolution des dépendances avec des modules externes, la gestion du versioning, les tests, etc. ;
 - **NodeJS&nbsp;:** Moteur d'applications orientées serveur basées sur le JavaScript ;
 - **JRE *(&laquo;&nbsp;Java Runtime Environment&nbsp;&raquo;)*** et **JDK *(« Java Development Kit »)*&nbsp;:** Moteur d'exécution et de développement d'applications Java.

Pour l'installation de la base de données HSS, l'utilisation d'un *overlay* exploitant le moteur de production *Apache Ant*. Comparable à *Apache Maven* ou encore *GNU Make*, Apache Ant fournit une aide à la compilation par la création de fichier de description détaillée (au format XML) comprenant notamment le processus de compilation, de résolution des dépendances avec des modules externes, la gestion du *versioning*, les tests, etc.

```bash
apt-get -fy install ant openjdk-7-jre openjdk-7-jdk node node-mysql libxml2 libxml2-dev libcurl4-gnutls-dev ipsec-tools libmysqld-dev libmysql-java libmysqld-dev mysql-client
```

Installation de MySQL-Server en mode non-interactif&nbsp;:

```bash
debconf-set-selections <<< "mysql-server mysql-server/root_password password ${DB_PASSWD}"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${DB_PASSWD}"
apt-get -y install mysql-server
```

### Installation de FHoSS

Clone des référentiels SVN du projet&nbsp;:

```bash
cd /opt/
mkdir -p /opt/OpenIMSCore/{FHoSS,ser_ims}/
svn checkout https://svn.code.sf.net/p/openimscore/code/FHoSS/trunk /opt/OpenIMSCore/FHoSS/
svn checkout https://svn.code.sf.net/p/openimscore/code/ser_ims/trunk /opt/OpenIMSCore/ser_ims/
```

Compilation de la base données HSS&nbsp;:

```bash
pushd /opt/OpenIMSCore/
pushd /opt/OpenIMSCore/FHoSS/
ant compile deploy 2>/dev/null
popd
```

### Installation d'OpenIMS

Compilation des services IMS (PCSCF, ICSCF, SCSCF)&nbsp;:

```bash
pushd /opt/OpenIMSCore/ser_ims/
make install-libs all 2>/dev/null
popd
```

Importation des données dans la base MySQL&nbsp;:

```bash
find /opt/OpenIMSCore/ \( -name "*.cfg" -o -name "*.xml" -o -name "*.sql" -o -name "*.sh" \) -exec sed -i 's/\(.*\)127.0.0.1\(.*\)/\1'${IP}'\2/; s/^\(listen=\).*$/\1'${IP}'/' {} \;
mysql -u root -p${DB_PASSWD} </opt/OpenIMSCore/FHoSS/scripts/hss_db.sql
mysql -u root -p${DB_PASSWD} </opt/OpenIMSCore/FHoSS/scripts/userdata.sql
mysql -u root -p${DB_PASSWD} </opt/OpenIMSCore/ser_ims/cfg/icscf.sql
```

### Configuration d'OpenIMS

Copie des scripts pour le démarrage des services IMS&nbsp;:

```bash
cp /opt/OpenIMSCore/ser_ims/cfg/*.cfg /opt/OpenIMSCore/
cp /opt/OpenIMSCore/ser_ims/cfg/*.xml /opt/OpenIMSCore/
cp /opt/OpenIMSCore/ser_ims/cfg/*.sh /opt/OpenIMSCore/
```

Configuration de l'environnement d'exécution de Java&nbsp;:

```bash
cat <<-'EOF' >>~/.bash_profile
# Répertoire d'accès à Java
JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64

EOF
source ~/.bash_profile
```

Définition du MD5 comme algorithme exploité pour l'authentification auprès du SCSCF&nbsp;:

```bash
sed -i '
    s/#*\(modparam("scscf","registration_default_algorithm","AKAv1-MD5")\)/#\1/;
    s/#*\(modparam("scscf","registration_default_algorithm","MD5")\)/\1/
' /opt/OpenIMSCore/scscf.cfg
```

Configuration de l'hôte et du port d'écoute pour le service Apache Tomcat de la base de données HSS&nbsp;:

```bash
sed -i 's/^\(host=\).*$/\1'${IP}'/; s/^\(port=\).*$/\18080/' /opt/OpenIMSCore/FHoSS/deploy/hss.properties
```

Installation de screen pour l'exécution parallèle des services IMS&nbsp;:

```bash
apt-get -fy install screen
```

Exécution des services IMS dans des *screens*&nbsp;:

```bash
pushd /opt/OpenIMSCore/
screen -S pcscf -dm bash -c "/opt/OpenIMSCore/pcscf.sh"
screen -S icscf -dm bash -c "/opt/OpenIMSCore/icscf.sh"
screen -S scscf -dm bash -c "/opt/OpenIMSCore/scscf.sh"
screen -S hss -dm bash -c "cd /opt/OpenIMSCore/FHoSS/deploy/; export JAVA_HOME=/usr; /opt/OpenIMSCore/FHoSS/deploy/startup.sh"
popd
screen -ls
```

**Notes&nbsp;:**

Identifiants par défaut pour l'accès au HSS&nbsp;:

| URL                | Nom d'utilisateur | Mot de passe |
|--------------------|-------------------|--------------|
| http://${IP}:8080/ | hssAdmin          | hss          |

Pour s'attacher à un *screen* en cours d'exécution il suffit de récupérer son identifiant et de s'y attacher, par exemple&nbsp;:

```bash
screen -r pcscf
```

Pour se détacher du *screen*, il suffit de presser la combinaison de touches `CTRL+A`, relâcher la combinaison puis presser la touche `D` *(&laquo;&nbsp;detach&nbsp;&raquo;)*.

### Installation du DNS Bind9

Installation&nbsp;:

```bash
apt-get -fy install bind9
systemctl stop bind9.service
```

Installation des zones DNS&nbsp;:

```bash
cat <<-EOF >/etc/bind/named.conf.local
zone "${DOMAIN}" {
    type master;
    file "/etc/bind/zones/${DOMAIN}.dnszone";
};

EOF
install -m 2775 -o root -g bind -d /etc/bind/zones/
cp /opt/OpenIMSCore/ser_ims/cfg/open-ims.dnszone /etc/bind/zones/${DOMAIN}.dnszone
```

Mise à jour des enregistrements de zone DNS&nbsp;:

```bash
sed -i 's/\(.*\)127.0.0.1\(.*\)/\1'${IP}'\2/' /etc/bind/zones/${DOMAIN}.dnszone
cat <<-EOF >>/etc/bind/zones/${DOMAIN}.dnszone
${HOSTNAME_IPBX_A}              1D IN A         ${IP_IPBX_A}
_sip.${HOSTNAME_IPBX_A}         1D SRV 0 0 5060 ${HOSTNAME_IPBX_A}
_sip._udp.${HOSTNAME_IPBX_A}    1D SRV 0 0 5060 ${HOSTNAME_IPBX_A}
_sip._tcp.${HOSTNAME_IPBX_A}    1D SRV 0 0 5060 ${HOSTNAME_IPBX_A}

${HOSTNAME_IPBX_B}              1D IN A         ${IP_IPBX_B}
_sip.${HOSTNAME_IPBX_B}         1D SRV 0 0 5060 ${HOSTNAME_IPBX_B}
_sip._udp.${HOSTNAME_IPBX_B}    1D SRV 0 0 5060 ${HOSTNAME_IPBX_B}
_sip._tcp.${HOSTNAME_IPBX_B}    1D SRV 0 0 5060 ${HOSTNAME_IPBX_B}

EOF
```

Vérification de la configuration et redémarrage de Bind9&nbsp;:

```bash
named-checkconf -z /etc/bind/named.conf
systemctl restart bind9.service
```

Mise à jour de la résolution de nom DNS locale&nbsp;:

```bash
cat <<-EOF >/etc/resolv.conf
search ${DOMAIN}
domain ${DOMAIN}
nameserver ${IP}
nameserver ${DNS1}
nameserver ${DNS2}

EOF
cp /etc/resolv.conf{,.ims}
```

### Ajout d'utilisateurs au réseau IMS

**Notes&nbsp;:**

| Paramètre   | Description                                                                                                                                                         |
|-------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| -u <IMSU>   | Nom d'utilisateur du réseau IMS                                                                                                                                     |
| -i <IMPI>   | Identité privée (associée à un ensemble d'identité, elle permet de décrire une identité globale associée, par exemple, à l'organisme titulaire de l'enregistrement) |
| -b <IMPU>   | Identité publique (associée à un utilisateur ou à une SDA)                                                                                                          |
| -t <TELURI> | Tel URI (aucune différence avec -b, en réalité il ne s'agit ni plus, ni moins que de l'implémentation d'une seconde IMPU)                                           |

#### IPBX A

Ajout d'un utilisateur pour l'IPBX A&nbsp;:

```bash
/opt/OpenIMSCore/ser_ims/cfg/add-imscore-user_newdb.sh -u ${HOSTNAME_IPBX_A} -i ${HOSTNAME_IPBX_A}@${DOMAIN} -b sip:${HOSTNAME_IPBX_A}@${DOMAIN} -r ${DOMAIN} -p 0000
sed -i '/^--.*/d' add-user-${HOSTNAME_IPBX_A}.sql
mysql -u root -p${DB_PASSWD} <./add-user-${HOSTNAME_IPBX_A}.sql
```

Ajout des SDA (IMPU) pour l'IPBX A&nbsp;:

```bash
for TEL_IMPU in ${TEL_IPBX_A}; do
mysql -u root -p${DB_PASSWD} < <(cat <<-EOF
```
```sql
/*
 * Récupération des identifiants contextuels
 */
SELECT @sp_id:=id FROM hss_db.sp ORDER BY id LIMIT 1;                                                          # Service Profile
SELECT @hostname_impu_id:=id FROM hss_db.impu WHERE hss_db.impu.identity='sip:${HOSTNAME_IPBX_A}@${DOMAIN}';   # Identité publique principale (IMPU) - p. ex. "sip:xivo@open-ims.test" - Identité pour l'appel (callable)
SELECT @visited_network_id:=id FROM hss_db.visited_network WHERE hss_db.visited_network.identity='${DOMAIN}';  # Visited Network - p. ex. "open-ims.test"
SELECT @impi_id:=id FROM hss_db.impi WHERE hss_db.impi.identity='${HOSTNAME_IPBX_A}@${DOMAIN}';                # Identité privée (IMPI) - p. ex. "xivo@open-ims.test" - Identité pour l'enregistrement

/*
 * Ajout d'une identité publique (IMPU)
 *
 * Exemple: sip:0549511000@open-ims.test
 */
INSERT INTO hss_db.impu(identity, id_sp, id_implicit_set) VALUES ('sip:${TEL_IMPU}@${DOMAIN}', @sp_id, @hostname_impu_id);
/*
 * Récupération de l'identifiant associé au nouvel IMPU
 */
SELECT @tel_impu_id:=id FROM hss_db.impu WHERE hss_db.impu.identity='sip:${TEL_IMPU}@${DOMAIN}';

/*
 * Association de l'identité publique à l'identité privée (IMPU -> IMPI)
 *
 * Exemple: sip:0549511000@open-ims.test -> xivo@open-ims.test
 */
INSERT INTO hss_db.impi_impu(id_impi, id_impu) VALUES (@impi_id, @tel_impu_id);

/*
 * Association de l'identité publique au Visited Network
 *
 * Exemple: sip:0549511000@open-ims.test -> open-ims.test
 */
INSERT INTO hss_db.impu_visited_network(id_impu, id_visited_network) VALUES (@tel_impu_id, @visited_network_id);

```
```bash
EOF
)
done
```

#### IPBX B

Ajout d'un utilisateur pour l'IPBX B&nbsp;:

```bash
/opt/OpenIMSCore/ser_ims/cfg/add-imscore-user_newdb.sh -u ${HOSTNAME_IPBX_B} -i ${HOSTNAME_IPBX_B}@${DOMAIN} -b sip:${HOSTNAME_IPBX_B}@${DOMAIN} -r ${DOMAIN} -p 0000
sed -i '/^--.*/d' add-user-${HOSTNAME_IPBX_B}.sql
mysql -u root -p${DB_PASSWD} <./add-user-${HOSTNAME_IPBX_B}.sql
```

Ajout des SDA (IMPU) pour l'IPBX B&nbsp;:

```bash
for TEL_IMPU in ${TEL_IPBX_B}; do
mysql -u root -p${DB_PASSWD} < <(cat <<-EOF
```
```sql
/*
 * Récupération des identifiants contextuels
 */
SELECT @sp_id:=id FROM hss_db.sp ORDER BY id LIMIT 1;                                                          # Service Profile
SELECT @hostname_impu_id:=id FROM hss_db.impu WHERE hss_db.impu.identity='sip:${HOSTNAME_IPBX_B}@${DOMAIN}';   # Identité publique principale (IMPU) - p. ex. "sip:xivo@open-ims.test" - Identité pour l'appel (callable)
SELECT @visited_network_id:=id FROM hss_db.visited_network WHERE hss_db.visited_network.identity='${DOMAIN}';  # Visited Network - p. ex. "open-ims.test"
SELECT @impi_id:=id FROM hss_db.impi WHERE hss_db.impi.identity='${HOSTNAME_IPBX_B}@${DOMAIN}';                # Identité privée (IMPI) - p. ex. "xivo@open-ims.test" - Identité pour l'enregistrement

/*
 * Ajout d'une identité publique (IMPU)
 *
 * Exemple: sip:0549511000@open-ims.test
 */
INSERT INTO hss_db.impu(identity, id_sp, id_implicit_set) VALUES ('sip:${TEL_IMPU}@${DOMAIN}', @sp_id, @hostname_impu_id);
/*
 * Récupération de l'identifiant associé au nouvel IMPU
 */
SELECT @tel_impu_id:=id FROM hss_db.impu WHERE hss_db.impu.identity='sip:${TEL_IMPU}@${DOMAIN}';

/*
 * Association de l'identité publique à l'identité privée (IMPU -> IMPI)
 *
 * Exemple: sip:0549511000@open-ims.test -> xivo@open-ims.test
 */
INSERT INTO hss_db.impi_impu(id_impi, id_impu) VALUES (@impi_id, @tel_impu_id);

/*
 * Association de l'identité publique au Visited Network
 *
 * Exemple: sip:0549511000@open-ims.test -> open-ims.test
 */
INSERT INTO hss_db.impu_visited_network(id_impu, id_visited_network) VALUES (@tel_impu_id, @visited_network_id);

```
```bash
EOF
)
done
```

Liste des utilisateurs&nbsp;:

```bash
mysql -u root -p${DB_PASSWD} <<< 'SELECT * FROM hss_db.imsu;'
```
