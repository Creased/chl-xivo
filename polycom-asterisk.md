# VoIP Asterisk / Polycom

## Infrastructure

### Réseau

<u>Plan d'adressage&nbsp;:</u> `172.28.128.0/24` pour commencer, mais les autres sous réseau `172.28.128.0/18` sont disponibles.

<u>VLAN&nbsp;:</u> `13`, présent uniquement sur le `sw-coeur` et `sw-admin-a`.

La prise `A3-0-3` est branchée sur le port `g3/0/13` de `sw-admin-a`.

<u>Route par défaut&nbsp;:</u> `172.28.128.1` cœur de réseau HP, instance sites-voip&nbsp;:

  - Le routage vers l'autocom (appliances A et B) Alcatel (`10.16.10.63`) est assuré. ping OK
  - Le routage vers le SBC ne semble pas bon, problème de route par défaut ??
  - Le routage vers les sites distants ne fonctionne pas non plus (le routeur Adista ne connait pas la route vers `172.28.128.0/24`)
  - Prise de main SSH depuis l'informatique&nbsp;: OK (on passe par le SNS qui connait la route)

<u>Serveur DHCP&nbsp;:</u> tout est configuré sauf... le serveur TFTP, les postes téléphoniques sont tous reconnus et déclarés dans le RADIUS. Les postes téléphoniques ont tous démarrés sans problèmes.

#### Configuration d'un port pour test avec poste téléphonique Polycom 

<u>Sur un HP A5120&nbsp;:</u>

```
<sw-admin-a>disp cur int g3/0/13
#
interface GigabitEthernet3/0/13
 description A3-0-3 Tests Asterisk
 port link-type hybrid
 undo port hybrid vlan 1
 port hybrid vlan 4 13 tagged
 port hybrid vlan 4000 untagged
 port hybrid pvid vlan 4000
 mac-vlan enable
 broadcast-suppression 20
 poe enable
 stp edged-port enable
 stp root-protection
 mac-authentication
 lldp voice-vlan 13
 dhcp-snooping information enable
 dhcp-snooping information remote-id string 00mil015
#

```

### Serveur IPBX

<u>asterix.miletrie.chl (hébergé dans le sandbox)&nbsp;:</u>

  - IP&nbsp;: 172.28.128.11 
  - Accès en SSH (ajouter la clé de Baptiste...)
  - Debian 8/64 à jour.
  - Apache 2.4 est installé pour tester le provisionnement via HTTP

### Postes téléphoniques

le VVX 300 doit être mis à jour&nbsp;! la version 4.1.7 n'est pas recommandée par Polycom. (5.1.3 c'est la version du moteur de mise à jour)

Les téléphones peuvent s'administrer via HTTP(s), le mot de passe administrateur est `456`.

On a pas mal d'infos sur le téléphone depuis le switch&nbsp;:

```
<sw-admin-a>disp lldp neighbor-information int g3/0/13

LLDP neighbor-information of port 125[GigabitEthernet3/0/13]:
  Neighbor index   : 2
  Update time      : 111 days,21 hours,4 minutes,13 seconds
  Chassis type     : Network address(ipv4)
  Chassis ID       : 172.28.128.250
  Port ID type     : MAC address
  Port ID          : 6416-7f91-478b
  Port description : 1
  System name        : Polycom VVX 300
  System description : Polycom;VVX-VVX_300;3111-46135-002,1;SIP/4.1.7.1428/13-Apr-16 18:22;UP/5.1.7.1368/13-Apr-16 18:59;
  System capabilities supported : Bridge,Telephone
  System capabilities enabled   : Telephone

  Management address type           : ipv4
  Management address                : 172.28.128.250
  Management address interface type : Unknown
  Management address interface ID   : Unknown
  Management address OID            : 0

  Auto-negotiation supported : Yes
  Auto-negotiation enabled   : Yes
  OperMau                    : speed(100)/duplex(Full)

  Device class: Endpoint Class III

  Media policy type        : Voice
  Unknown Policy           : No
  VLAN tagged              : Yes
  Media policy VlanID      : 13
  Media policy L2 priority : 5
  Media policy Dscp        : 46

  Media policy type        : Voice Signaling
  Unknown Policy           : No
  VLAN tagged              : Yes
  Media policy VlanID      : 13
  Media policy L2 priority : 5
  Media policy Dscp        : 44

  HardwareRev               : 3111-46135-002,1
  FirmwareRev               : UP/5.1.7.1368/13-Apr-16 18:59
  SoftwareRev               : SIP/4.1.7.1428/13-Apr-16 18:22
  SerialNum                 : 64167f91478b
  Manufacturer name         : Polycom
  Model name                : VVX-VVX_300

  PoE PD power source       : PSE and local
  Port PD Priority          : Unknown
  Port available power value: 5.0(w)

```
